//
//  SignUpController.swift
//  LoginSignUpReusable
//
//  Created by Pooja Rana on 15/03/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit
import WebKit

class SignUpController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource  {

    // MARK: -  IBOutlet 
    @IBOutlet weak var firstNameText           :CustomAnimatableTextfield!
    @IBOutlet weak var emailText               :CustomAnimatableTextfield!
    @IBOutlet weak var addressText             :CustomAnimatableTextfield!
    @IBOutlet weak var countryText             :CustomAnimatableTextfield!
    @IBOutlet weak var stateText               :CustomAnimatableTextfield!
    @IBOutlet weak var cityText                :CustomAnimatableTextfield!
    @IBOutlet weak var zipCodeText             :CustomAnimatableTextfield!
    @IBOutlet weak var passwordText            :CustomAnimatableTextfield!
    @IBOutlet weak var confirmPasswordText     :CustomAnimatableTextfield!
    
    @IBOutlet var termsView: UIView!
    @IBOutlet var termsWebView: WKWebView!
   
    var listView = UIPickerView()
    var countryArray = [CountryModel]()
    var selectedcountry : CountryModel?
    var isCountry = true

    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        listView.delegate = self
        let allListView = EventManager.inputV(listView, sender: self)
        setAsInputView(self.countryText, list: allListView)
        setAsInputView(self.stateText, list: allListView)
        
        self.countryArray = [getCountry(name: "India", stateArr: ["Punjab", "Himachal", "Haryana"]),
                                 getCountry(name: "Canada", stateArr: ["Ontario", "Alberta", "Nova Scotia"]),
                                 getCountry(name: "USA", stateArr: ["California", "Florida", "Texas"])]
    }

    func setAsInputView(_ textField: CustomAnimatableTextfield, list: UIView) {
        textField.inputView = list
        textField.tintColor = .clear
    }
    
    func getCountry(name: String, stateArr: [String]) -> CountryModel {
        let country = CountryModel()
        country.country = name
        country.states = stateArr
        return country
    }
    
    // MARK: -  IBOutlet Action 
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectCountryAction(_ sender: UIButton) {
        isCountry = true
        self.view.endEditing(true)
        
        if countryArray.count > 0 {
            listView.reloadAllComponents()
            if selectedcountry == nil {
                listView.selectRow(0, inComponent: 0, animated: true)
            } else {
                let indexOfElement = countryArray.index(of: selectedcountry!)
                listView.selectRow(indexOfElement!, inComponent: 0, animated: true)
            }
            self.countryText.becomeFirstResponder()
            
        } else {
            EventManager.showAlert(alertMessage: ConstantModel.message.NoCountry, btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil)
        }
    }
    
    @IBAction func selectStateAction(_ sender: UIButton) {
        isCountry = false
        self.view.endEditing(true)
        if self.selectedcountry != nil {
            listView.reloadAllComponents()
            if stateText.text == "" {
                listView.selectRow(0, inComponent: 0, animated: true)
                
            } else {
                let arr = selectedcountry?.states?.filter { $0 == stateText.text }
                if (arr?.count)! > 0 {
                    let state = arr?[0]
                    let indexOfElement = selectedcountry?.states?.index(of: state!)
                    listView.selectRow(indexOfElement!, inComponent: 0, animated: true)
                } else {
                    listView.selectRow(0, inComponent: 0, animated: true)
                }
            }
            stateText.becomeFirstResponder()
        } else {
            EventManager.showAlert(alertMessage: ConstantModel.message.SelectCountryFirst, btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil)
        }
    }
    
    @IBAction func registerAction(_ sender: UIButton) {
        if self.validateSignUp(){ self.registerAPI() }
    }
    
    @IBAction func checkMyTermsAndPrivacyAction(_ sender: UIButton) {
        let window = UIApplication.shared.keyWindow!
        self.termsView.frame = window.frame
        window.addSubview(termsView)
        
       //100 for Terms button and 200 for privacy policy
        let url = URL(string: sender.tag == 100 ? "http://www.google.com" : "http://www.fb.com")
        let request = URLRequest(url:url!)
        termsWebView.load(request)
    }
    @IBAction func closeMyTermsAndPrivacyAction(_ sender: UIButton) {
        termsView.removeFromSuperview()
    }
    @objc func resignTextField(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        
        if sender.tag == 101 {
            if isCountry {
                selectedcountry = countryArray[countryText.tag]
                countryText.text = selectedcountry?.country
                stateText.text = ""
            } else {
                let state = selectedcountry?.states?[stateText.tag]
                stateText.text = state
            }
        }
    }
    
  
    
    // MARK: -  UIPicker Delegates 
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return isCountry ? countryArray.count : (selectedcountry?.states?.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return isCountry ? countryArray[row].country : selectedcountry?.states?[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if isCountry {
            countryText.tag = row
            stateText.tag = 0
        } else {
            stateText.tag = row
        }
    }
    
    // MARK: -  API Call 
    func registerAPI() {
        //Perform SignUp API & then navigate to Home
        ApiCallModel.shared.apiCall(delegate: self) {
            self.performSegue(withIdentifier: "showHomeFromSignUp", sender: nil)
        }
    }
}
