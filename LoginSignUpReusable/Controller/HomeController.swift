//
//  HomeController.swift
//  LoginSignUpReusable
//
//  Created by Pooja Rana on 15/03/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    
    
    // MARK: -  IBOutlet Action 
    @IBAction func logoutButtonAction(_ sender: UIButton) {
        self.LogoutAPI()
    }
    
    // MARK: -  API Call 
    func LogoutAPI() {
        //Perform Logout API & then Go to Login
        ApiCallModel.shared.apiCall(delegate: self) {
            let storyboard      = UIStoryboard(name: "Main", bundle: Bundle.main)
            let rootNav    = storyboard.instantiateViewController(withIdentifier: "rootNav")
            ConstantModel.appDelegate.window!.rootViewController = rootNav
        }
    }
}
