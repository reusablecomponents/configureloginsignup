//
//  ForgotPasswordController.swift
//  LoginSignUpReusable
//
//  Created by Pooja Rana on 15/03/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

class ForgotPasswordController: UIViewController {

    // MARK: -  IBOutlet 
    @IBOutlet weak var emailText : CustomAnimatableTextfield!
    
    // MARK: -  IBOutlet Action 
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        if self.validateEmail(){
            self.ForgotPasswordAPI()
        }
    }
    
    // MARK: -  API Call 
    func ForgotPasswordAPI() {
        //Perform ForgotPassword API & then Pop back to Login
        ApiCallModel.shared.apiCall(delegate: self) {
            EventManager.showAlert(alertMessage: "Password has been sent to your email.", btn1Tit: "Ok", btn2Tit: nil, sender: self, action: { _ in
                self.navigationController?.popViewController(animated: true)
            })
        }
    }

}
