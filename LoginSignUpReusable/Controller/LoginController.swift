//
//  LoginController.swift
//  LoginSignUpReusable
//
//  Created by Pooja Rana on 15/03/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    // MARK: -  IBOutlet 
    @IBOutlet weak var emailField       : CustomAnimatableTextfield!
    @IBOutlet weak var passwordField    : CustomAnimatableTextfield!
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: -  IBOutlet Action 
    @IBAction func loginButtonAction(_ sender: UIButton) {
        if self.validateLogin() {
            self.LoginAPI()
        }
    }
    
    // MARK: -  API Call 
    func LoginAPI() {
        //Perform Login API & then navigate to Home
        ApiCallModel.shared.apiCall(delegate: self) {
            self.performSegue(withIdentifier: "showHomeFromLogin", sender: nil)
        }
    }
}
