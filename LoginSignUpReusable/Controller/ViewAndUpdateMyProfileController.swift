//
//  ViewAndUpdateMyProfileController.swift
//  LoginSignUpReusable
//
//  Created by Sakshi on 01/05/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//
import UIKit

class ViewAndUpdateMyProfileController: UIViewController {
    
    //IBOutlets
    @IBOutlet weak var MyDpImageView: UIImageView!
    

    //MARK:- Variable decleration
    //URL containing the image
    let URL_IMAGE = URL(string: "http://www.simplifiedtechy.net/wp-content/uploads/2017/07/simplified-techy-default.png")
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyDpImageView.downloadedFrom(url: URL_IMAGE!)
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        self.imagePicker.modalPresentationStyle = .popover
        
    }
    
    @IBAction func actionChangeProfilePhoto(_ sender: UIButton) {
        EventManager.showAlertWithMultipleButtons(
            alertMessage: ConstantModel.message.SelectCountryFirst,
            buttonTitles: ["Open Camera", "Import from Photos"],
            sender: self,
            action: { title in
                
                if title == "Open Camera" {
                    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                        self.imagePicker.sourceType = .camera
                    }
                    
                } else {
                    self.imagePicker.sourceType = .photoLibrary
                }
                
                self.present(self.imagePicker, animated: true, completion: nil)

        },
            style: 1)
    }
    
}
