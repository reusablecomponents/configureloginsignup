//
//  LoginSignUpValidation.swift
//  LoginSignUpReusable
//
//  Created by Pooja Rana on 31/08/17.
//  Copyright © 2017 Pooja Rana. All rights reserved.
//

import Foundation
import UIKit

extension LoginController {
    
    func validateLogin() -> Bool {
        self.view.endEditing(true)
        if emailField.text!.isStringEmpty() || passwordField.text!.isStringEmpty() {
            EventManager.showAlert(alertMessage: ConstantModel.message.FillAllField, btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil)
            return false
            
        } else {
            if (emailField.text?.isNotValidateEmailId())! {
                EventManager.showAlert(alertMessage: ConstantModel.message.ValidEmail, btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil)
                return false
            } else {
                return true
            }
        }
    }
}

extension ForgotPasswordController {
    
    func validateEmail() -> Bool {
        self.view.endEditing(true)
        if emailText.text!.isStringEmpty() {
            EventManager.showAlert(alertMessage: ConstantModel.message.EnterEmail, btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil)
            return false
            
        } else {
            if (emailText.text?.isNotValidateEmailId())! {
                EventManager.showAlert(alertMessage: ConstantModel.message.ValidEmail, btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil)
                return false
                
            } else {
                return true
            }
        }
    }
}

extension SignUpController {
    
    func validateSignUp() -> Bool {
        self.view.endEditing(true)
        if  firstNameText.text!.isStringEmpty() ||
            emailText.text!.isStringEmpty() ||
            addressText.text!.isStringEmpty() ||
            countryText.text!.isStringEmpty() ||
            stateText.text!.isStringEmpty() ||
            cityText.text!.isStringEmpty() ||
            zipCodeText.text!.isStringEmpty() ||
            passwordText.text!.isStringEmpty() ||
            confirmPasswordText.text!.isStringEmpty() {
            EventManager.showAlert(alertMessage: ConstantModel.message.FillAllField, btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil)
            return false
            
        } else if (emailText.text?.isNotValidateEmailId())! {
            EventManager.showAlert(alertMessage: ConstantModel.message.ValidEmail, btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil)
            return false
            
        } else if passwordText.text != confirmPasswordText.text {
            EventManager.showAlert(alertMessage: ConstantModel.message.MatchPassword, btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil)
            return false
            
        }
        return true
    }
}

extension ViewAndUpdateMyProfileController : UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        // use the image
        MyDpImageView.image = chosenImage
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        self.view.bringSubview(toFront: scrollView)
        return MyDpImageView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        self.view.sendSubview(toBack: scrollView)
        
        scrollView.setZoomScale(1, animated: true)
    }
}

extension UIImageView {
    
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}
