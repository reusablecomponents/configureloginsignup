//
//  CustomAnimatableImageView.swift
//  LoginSignUpReusable
//
//  Created by Sakshi on 01/05/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

@IBDesignable public class CustomAnimatableImageView: UIImageView {
    
    @IBInspectable public var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable public var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            self.clipsToBounds = true
        }
    }
    
}
