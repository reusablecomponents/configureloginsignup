//
//  CustomAnimatableTextfield.swift
//  LoginSignUpReusable
//
//  Created by Pooja Rana on 31/08/17.
//  Copyright © 2017 Pooja Rana. All rights reserved.
//

import UIKit

class CustomAnimatableTextfield: UITextField, UITextFieldDelegate {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.delegate = self
    }
    
    @IBInspectable internal var maxLength: Int = 0
    
    // MARK: - Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        let newLength = currentCharacterCount + string.count - range.length
        
        if textField.isSecureTextEntry {
            if (string == " ") {
                return false
            }
        }
        
        if maxLength != 0 {
            return newLength <= maxLength
        } else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTage      = textField.tag+1
        // Try to find next responder
        let nextResponder = textField.superview?.viewWithTag(nextTage) as UIResponder!
        if (nextResponder != nil) {
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
            
        } else {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        return true // We do not want UITextField to insert line-breaks.
    }
}
