//
//  ApiCallModel.swift
//  Chatmate
//
//  Created by Pooja Rana on 13/04/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

typealias Callback    = () -> Void

class ApiCallModel: NSObject {
    
    static let shared       = ApiCallModel()
    private override init() { }

    func apiCall(delegate: UIViewController, success: @escaping Callback) {
        if EventManager.checkForInternetConnection() {
            EventManager.showloader()
            
            let when = DispatchTime.now() + 2.0
            DispatchQueue.main.asyncAfter(deadline: when) {
                EventManager.hideloader()
                success()
            }
        } else {
            EventManager.showAlert(alertMessage: ConstantModel.api.NoInternet,
                                   btn1Tit: "Ok",
                                   btn2Tit: nil,
                                   sender: delegate,
                                   action: nil)
        }
    }
}
