LoginSignUpReusable works on Swift4.0 and requires ARC to build. It depends on the following Apple frameworks, which should already be included with most Xcode templates:

Foundation.framework
UIKit.framework

Alternatively you can directly add the LoginSignUpValidation.swift and StringValidationExtension.swift source files to your project.

You can use validations by using below methods:
1. validateLogin()
2. validateEmail()
3. validateSignUp()


To upload picture or update it use "ViewAndUpdateMyProfileController" Controller. You can find it in main.storyboard


